import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HW 4: Animations',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: LogoApp(),
    );
  }
}

class LogoApp extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<LogoApp> with SingleTickerProviderStateMixin {

  AnimationController controller;
  Animation animation;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(
          vsync: this, duration: Duration(milliseconds: 1400),);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    controller.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });

    controller.forward();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HW 4: Animations"),
      ),

      body: Center(
          child:
          FadeTransition(
            opacity: animation,
            child: Container(
              width: 300,
              height: 300,
              decoration: BoxDecoration(
                  image: new DecorationImage(
                      image: new AssetImage(
                        'images/casper.png',
                      )
                  )
              ),
            ),
          )
      ),
    );
  }
}